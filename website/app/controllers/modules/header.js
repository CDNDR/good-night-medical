(function () {
  'use strict';

  var controllerId = 'header';
  angular.module('app').controller(controllerId, ['$scope', headerCtrl]);

  function headerCtrl($scope) {
    $scope.phoneNumber = phoneNumber;
    $scope.navMenu = false;
    $scope.linksState = linksState;
    $scope.linksSocial = linksSocial;

    $scope.showMenu = function() {$scope.navMenu = true;}
    $scope.hideMenu = function() {$scope.navMenu = false;}
  }

  var phoneNumber = '877-753-3742';

  var linksState = [{
      bind: 'home',
      sref: 'home'
    },{
      bind: 'about us',
      sref: 'information.aboutUs'
    },{
      bind: 'locations',
      sref: 'information.locations'
    },{
      bind: 'rent',
      sref: 'rent.catalog'
    },{
      bind: 'auto re-supply',
      sref: 'reSupply.info'
    },{
      bind: 'home sleep',
      sref: 'sleepTest.info'
    },{
      bind: 'testimonials',
      sref: 'information.testimonials'
    }];

  var linksSocial = [{
      bind: 'facebook',
      href: '//facebook.com/goodnightmed/'
    },{
      bind: 'twitter',
      href: '//twitter.com/goodnightmed'
    },{
      bind: 'pinterest',
      href: '//pinterest.com/goodnightmed/'
    }];

})();

