(function () {
  'use strict';

  var controllerId = 'thanks';
  angular.module('app').controller(controllerId, ['$scope', thanksCtrl]);

  function thanksCtrl($scope) {
    $scope.content = content;
  }

  var content = [{
    title: 'Shipping',
    firstName: 'Firt Name',
    lastName: 'Last Name',
    address: 'Address',
    addressOpt: 'Address opt',
    state: 'State',
    zip: 12345
  },{
    title: 'Billing',
    firstName: 'Firt Name',
    lastName: 'Last Name',
    address: 'Address',
    addressOpt: '',
    state: 'State',
    zip: 12345
  }];

})();

