(function () {
  'use strict';

  var controllerId = 'billing';
  angular.module('app').controller(controllerId, ['$scope', 'dataService', billingCtrl]);

  function billingCtrl($scope, dataService) {
    $scope.months = months;
    $scope.years = years;
    $scope.states = states;
    $scope.purchaseResult = purchaseResult;
    $scope.purchaseInfo = purchaseInfo;

    $scope.sameBillShip = true;
   
    $scope.submitPurchase = function(){
      if ($scope.billData.$valid) {
        // Здесь должна быть функция отправки данных
        // Все ng-model данных в форме
        //
        // Адрес доставки
        // $scope.shipFirstName  - Имя
        // $scope.shipLastName   - Фамилия
        // $scope.shipAddress    - Адрес первый (обязательный)
        // $scope.shipAddressOpt - Адрес второй (опциональный)
        // $scope.shipCity       - Город
        // $scope.shipState      - Штат
        // $scope.shipZip        - Индекс
        //
        // Данные карточки
        // $scope.cardNumber     - Номер
        // $scope.cardMonth      - Месяц
        // $scope.cardYear       - Год
        // $scope.cardCvv        - CVV код
        //
        // $scope.sameBillShip   - Если TRUE то платежный адрес такой же как и отправки (данные копируются с адреса доставки), FALSE адрес отличается (другой)
        //
        // $scope.billFirstName  - Имя
        // $scope.billLastName   - Фамилия
        // $scope.billAddress    - Адрес первый (обязательный)
        // $scope.billAddressOpt - Адрес второй (опциональный)
        // $scope.billCity       - Город
        // $scope.billState      - Штат
        // $scope.billZip        - Индекс
        //
        // Промоко код на скидки
          // $scope.promoCode - Пока ничего про него не известно, так что пропускай

          var shipmentModel = {
              firstName: $scope.shipFirstName,
              lastName: $scope.shipLastName,
              primaryAddress: $scope.shipAddress,
              secondaryAddress: $scope.shipAddressOpt,
              city:$scope.shipCity,
              state:$scope.shipState,
              zip: $scope.shipZip
          };
          var creditCardModel = {
              cardNumber:$scope.cardNumber,
              expirationMonth:$scope.cardMonth,
              expirationYear: $scope.cardYear,
              cvv: $scope.cardCvv,
          };
          var billingModel = {
              firstName: $scope.billFirstName,
              lastName: $scope.billLastName,
              primaryAddress: $scope.billAddress,
              secondaryAddress: $scope.billAddressOpt,
              city: $scope.billCity,
              state: $scope.billState,
              zip: $scope.billZip
          };
          var paymentModel = {
              shipment: shipmentModel,
              creditCard: creditCardModel,
              billing: billingModel,
              sameBillShip: $scope.sameBillShip
          };

          dataService.postPayment(paymentModel).success(function () {
              //some success behavior 
          });
      }
    };
   

      // Считает purchaseResult
    $scope.purchaseResultTotal = function() {
      var sum = 0;
      for (var i = 0; i < purchaseResult.length; i++) sum += purchaseResult[i].cost;
      return '$' + sum;
    }
  }

  var purchaseResult = [{
    bind: 'Sub Total',
    cost: 75
  },{
    bind: 'Tax',
    cost: 1.12
  }];
  
  var months = [1,2,3,4,5,6,7,8,9,10,11,12];
  var years  = [];
  for (var i = 0; i < 21; i++) years.push(new Date().getFullYear() + i);

  var purchaseInfo = '*By clicking “Purchase” you understand that your insurance and/or prescription still need to be approved. If for any reason we are not able to process your order due to insurance or lack of valid prescription, we will refund your money in full.';

  var states = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"];

})();






















