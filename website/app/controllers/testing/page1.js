(function() {
  'use strict';

  var controllerId = 'testPage1';
  angular.module('app').controller(controllerId, ['$scope', testPage1Ctrl]);

  function testPage1Ctrl($scope) {
    $scope.states = states;
    $scope.options = options;

    $scope.setOption = function(opt) {
      $scope.testSelect = opt.title;
    }

    $scope.showOptions = function() {
      $scope.listOptions = !$scope.listOptions;
    }
  }

  var states = [
    'Alabama',
    'Alaska',
    'Arizona',
    'Arkansas',
    'California',
    'Colorado',
    'Connecticut',
    'Delaware',
    'Florida',
    'Georgia',
    'Hawaii',
    'Idaho',
    'Illinois',
    'Indiana',
    'Iowa',
    'Kansas',
    'Kentucky',
    'Louisiana',
    'Maine',
    'Maryland',
    'Massachusetts',
    'Michigan',
    'Minnesota',
    'Mississippi',
    'Missouri',
    'Montana',
    'Nebraska',
    'Nevada',
    'New Hampshire',
    'New Jersey',
    'New Mexico',
    'New York',
    'North Carolina',
    'North Dakota',
    'Ohio',
    'Oklahoma',
    'Oregon',
    'Pennsylvania',
    'Rhode Island',
    'South Carolina',
    'South Dakota',
    'Tennessee',
    'Texas',
    'Utah',
    'Vermont',
    'Virginia',
    'Washington',
    'West Virginia',
    'Wisconsin',
    'Wyoming'
  ];

  var options = [{
    title: 'Option Title 1',
    description: 'Option Text 1',
    imgUrl: 'OPT 1',
    color: '000'
  },{
    title: 'Option Title 2',
    description: 'Option Text 2',
    imgUrl: 'OPT 2',
    color: '000'
  },{
    title: 'Option Title 3',
    description: 'Option Text 3',
    imgUrl: 'OPT 3',
    color: '000'
  },{
    title: 'Option Title 4',
    description: 'Option Text 4',
    imgUrl: 'OPT 4',
    color: '000'
  },{
    title: 'Option Title 5',
    description: 'Option Text 5',
    imgUrl: 'OPT 5',
    color: '000'
  },{
    title: 'Option Title 6',
    description: 'Option Text 6',
    imgUrl: 'OPT 6',
    color: '000'
  },{
    title: 'Option Title 7',
    description: 'Option Text 7',
    imgUrl: 'OPT 7',
    color: '000'
  },{
    title: 'Option Title 8',
    description: 'Option Text 8',
    imgUrl: 'OPT 8',
    color: '000'
  },{
    title: 'Option Title 9',
    description: 'Option Text 9',
    imgUrl: 'OPT 9',
    color: '000'
  },{
    title: 'Option Title 10',
    description: 'Option Text 10',
    imgUrl: 'OPT 10',
    color: '000'
  }];

})();
