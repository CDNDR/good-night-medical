(function () {
  'use strict';

  var controllerId = 'block4';
  angular.module('app').controller(controllerId, ['$scope', block4Ctrl]);

  function block4Ctrl($scope) {
    $scope.content = content;
  }

  var content = [{
    title: 'what is sleep apnea?',
    desc: 'Sleep apnea is a chronic sleep disorder found most commonly in men, and women after menopause. Symptoms include: loud snoring, waking up gasping for air, choking or coughing and excessive daytime tiredness. If not properly treated sleep apnea can lead to high blood pressure, heart problems, stroke, Type 2 diabetes, metabolic syndrome and liver issues',
    class: 'knight6'
  },{
    title: 'screening tool',
    desc: 'Do you constantly feel tired throughout your day? Take this short quiz to find out if you are at risk of having a sleep disorder',
    class: 'knight3'
  },{
    title: 'what good night medical can do for you!',
    desc: 'At Good Night Medical, we strive to be the best Durable Medical Equipment supplier in the nation. We offer top-of-the-line customer service that will talk to you every step of the way to help you overcome your battle with sleep apnea. Our services range from scheduling a home sleep test for you, setting up your CPAP supplies, and making sure you are never short on new, clean supplies',
    class: 'knight5'
  }];

})();

