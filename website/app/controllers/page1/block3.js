(function () {
  'use strict';

  var controllerId = 'block3';
  angular.module('app').controller(controllerId, ['$scope', block3Ctrl]);

  function block3Ctrl($scope) {
    $scope.content = content;
  }

  var content = {
    title: 'Three Simple Steps to a Better Sleeping You',
    steps: [{
      title: 'Enter your email above',
      desc: 'We will we send you a PDF-form to your email or ask for a Home Sleep Test Order form to be mailed to you'
    },{
      title: 'Print the form',
      desc: 'Fill out the patient information section either electronically or once printed. The rest of the form will be completed by your doctor. Be sure to get the signature at the bottom!'
    },{
      title: 'Receive your sleep device',
      desc: 'We will send you a home sleep test that you will wear for three nights. A home sleep test specialist will call you throughout your test to see if you need any assistance'
    }]
  };

})();

