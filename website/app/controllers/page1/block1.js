(function () {
  'use strict';

  var controllerId = 'block1';
  angular.module('app').controller(controllerId, ['$scope', 'dataService', block1Ctrl]);

  function block1Ctrl($scope, dataService) {
    $scope.content = content;
    $scope.link = link;
    $scope.contactUsInfo = {};

    $scope.submitContactUsForm = function(){
      if ($scope.contactUsForm.$valid) {
          dataService.postContactUs($scope.contactUsInfo)
            .success(function () {
              //clear and reset the form after submit
              $scope.contactUsInfo = {};
              $scope.contactUsForm.$setUntouched();
              $scope.contactUsForm.$setPristine();
          });
      }
    };

    $scope.returnHeight = function() {
      return { height: window.innerHeight + 40 + 'px' };
    };
  }

  var content = {
    title: 'Think You or Your Spouse has Sleep Apnea?',
    desc1: 'Enter your contact information below and receive details on our convenient home sleep testing.',
    desc2: 'It’s simple, easy, and sent directly to your door!'
  };

  var link = {
    bind1: 'Click here',
    bind2: 'to learn more about home sleep testing',
    uiSref: 'sleepTest.info'
  };

})();

