(function () {
  'use strict';

  var controllerId = 'catalogCpap';
  angular.module('app').controller(controllerId, ['$scope', 'dataService', catalogCpapCtrl]);

  function catalogCpapCtrl($scope, dataService) {
    $scope.range = range;
    $scope.filters = filters;
    $scope.links = links;
    $scope.machines = [];
    
    dataService.getMachinesCatalog().success(function (data) {
      $scope.machines = data;
    });
    
  }

  var range = [{
    name: 'Price', // Заголовок ползунка
    minCost: 0,    // Мин. выбранная цена
    maxCost: 75,   // Мак. выбранная цена
    min: 0,        // Мин. возможная цена
    max: 75        // Мак. возможная цена
  }];

  var filters = [
    [{
      title: 'Refurbished',
      model: ''
    }],
    [{
      title: 'Philips',
      model: ''
    },{
      title: 'ResMed',
      model: ''
    },{
      title: 'Fisher & Paykel',
      model: ''
    }]
  ];

  var links = [{
    bind: 'Let Us Help You Pick A CPAP',
    sref: 'catalog.help'
  }];

})();

