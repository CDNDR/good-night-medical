(function () {
  'use strict';

  var controllerId = 'catalogHelpPick';
  angular.module('app').controller(controllerId, ['$scope', catalogHelpPickCtrl]);

  function catalogHelpPickCtrl($scope) {
    $scope.links = links;

    $scope.currentPatient = true;
    $scope.diagnosedSleep = false;
    $scope.sleepStudy = false;
    $scope.prescription = false;
    $scope.currentMachine = 'Other';

    $scope.resetForm = function() {
      // TO DO: Нужно сделать
    };

    $scope.submitHelpPick = function(){
      if ($scope.helpPick.$valid) {
        // Здесь должна быть функция отправки данных
        // Все ng-model данных в форме
        //
        // $scope.name  - Имя Фамилия (обязательное)
        // $scope.email - Почта       (обязательное)
        // $scope.phone - Номер       (обязательное)
        //
        // $scope.currentPatient - Пациент GNMK (от нее зависят следующие поля)
        // - YES { NOTHING }
        // - NO  {
        // -- $scope.address   - Адрес         (обязательное)
        // -- $scope.dateBirth - Дата рождения
        //
        // -- $scope.diagnosedSleep - Диагности... (от нее зависят следующие поля)
        // --- NO  { NOTHING }
        // --- YES {
        // ---- $scope.currentMachine   - Текущая машина
        // ---- $scope.currentEquipment - Текушие оборудование (обязательное)
        // ---- $scope.currentMask      - Текущая маска        (обязательное)
        // ----
        // ---- $scope.sleepStudy - Копия иследования (от нее зависят следующие поля)
        // ----- YES { $scope.fileSleepStudy - Изображение / Скан  }
        // ----- NO  { $scope.sleepTest      - Какая то информация }
        // ----
        // ---- $scope.prescription - Рецепт (от нее зависят следующие поля)
        // ----- NO or UNSURE  { NOTHING }
        // ----- YES           { $scope.filePrescription - Документ, Скан рецепта }
        // --- }
        // - }
        //
        // $scope.message - Сообщение (любой текст от пользователя, пожелания или еще чего)
      }
    };
  }

  var links = [{
    bind: 'Let Us Help You Pick A CPAP',
    sref: 'catalog.help'
  },{
    bind: 'Catalog',
    sref: 'catalog.cpap'
  }];

})();

