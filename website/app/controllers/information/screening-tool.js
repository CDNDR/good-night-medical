(function () {
  'use strict';

  var controllerId = 'screeningTool';
  angular.module('app').controller(controllerId, ['$scope', screeningToolCtrl]);

  function screeningToolCtrl($scope) {
    $scope.radio = radio;
  };

  var radio = [{
    label: 'Do you snore?',
    model: 'radio1',
    name: 'radio1',
    answers: [
      'Yes',
      'No',
      'Dont Know'
    ]
  },{
    label: 'If you snore, your snoring is...',
    model: 'radio2',
    name: 'radio2',
    answers: [
      'Slightly louder than breathing',
      'As loud as talking',
      'Louder than talking',
      'Very loud - can be heard in adjacent rooms'
    ]
  },{
    label: 'How often do you snore?',
    model: 'radio3',
    name: 'radio3',
    answers: [
      'Nearly every day',
      '3-4 times a week',
      '1-2 times a week',
      '1-2 times a month',
      'Never or nearly never'
    ]
  },{
    label: 'Has your snoring ever bothered other people?',
    model: 'radio4',
    name: 'radio4',
    answers: [
      'Yes',
      'No',
      'Dont Know'
    ]
  },{
    label: 'Has anyone noticed that you quit breathing during your sleep?',
    model: 'radio5',
    name: 'radio5',
    answers: [
      'Nearly every day',
      '3-4 times a week',
      '1-2 times a week',
      '1-2 times a month',
      'Never or nearly never'
    ]
  },{
    label: 'How often do you feel tired or fatigued after your sleep?',
    model: 'radio6',
    name: 'radio6',
    answers: [
      'Nearly every day',
      '3-4 times a week',
      '1-2 times a week',
      '1-2 times a month',
      'Never or nearly never'
    ]
  },{
    label: 'During your waking time, do you feel tired, fatigued, or not up to par?',
    model: 'radio7',
    name: 'radio7',
    answers: [
      'Nearly every day',
      '3-4 times a week',
      '1-2 times a week',
      '1-2 times a month',
      'Never or nearly never'
    ]
  },{
    label: 'Have you ever nodded off or fallen asleep while driving a vehicle?',
    model: 'radio8',
    name: 'radio8',
    answers: [
      'Yes',
      'No'
    ]
  },{
    label: 'If yes, how often does it occur?',
    model: 'radio9',
    name: 'radio9',
    answers: [
      'Nearly every day',
      '3-4 times a week',
      '1-2 times a week',
      '1-2 times a month',
      'Never or nearly never'
    ]
  },{
    label: 'Do you have high blood pressure?',
    model: 'radio10',
    name: 'radio10',
    answers: [
      'Yes',
      'No',
      'Dont Know'
    ]
  }];

})();


















