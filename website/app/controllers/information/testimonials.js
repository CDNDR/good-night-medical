(function () {
  'use strict';

  var controllerId = 'testimonials';
  angular.module('app').controller(controllerId, ['$scope', testimonialsCtrl]);

  function testimonialsCtrl($scope) {
    $scope.content = content;
  };

  var content = [{
    who: 'Dan & Bea',
    place: 'Columbus, OH',
    comment: [
      'My wife and I are big believers in the CPAP system. I am 80 and have been using it for over 6 years. As I aged I started to experience restless leg syndrome which interfered with sleep. By using the CPAP machine I no longer shake our king sized bed. Now we both get a night of restful sleep which keeps our energy high throughout the day. A life and a wife saver!'
    ]
  },{
    who: 'JC',
    place: 'Redmond, WA',
    comment: [
      'Over the past six weeks I have been trying to acquire a new CPAP. On my MD’s recommendation I called a local company, then another and another, each more irresponsible, incompetent and unresponsive than the other. And then I called Stat Medical, now Good Night Medical.',
      'They actually answered their phone! They were knowledgeable and informative and get this, when “my” representative, Brandy, did not have an answer to a question, she asked me to hold while she found a tech who could answer. Imagine that, an honest salesperson! Brandy is a gem, courteous, professional – a member of the Good Night Medical staff! I hasten to add, the technician who helped me was very informed, patient, and very understanding.',
      'I highly recommend this company and their devoted staff who are truly customer service oriented professionals!',
      'No idiopathic nonsense here.',
      'With profound appreciation.'
    ]
  }];

})();
