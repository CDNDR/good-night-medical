(function () {
  'use strict';

  var controllerId = 'reSupplyForm';
  angular.module('app').controller(controllerId, ['$scope', 'dataService',reSupplyFormCtrl]);

  function reSupplyFormCtrl($scope,dataService) {
    $scope.months = months;
    $scope.days = days;
    $scope.years = years;

    $scope.currentPatient = true;

    // Отправляет данные, если все поля заполнены верно (функция вызывается с помощью ng-submit)
    $scope.submitData = function(){
      if ($scope.reSupply.$valid) {
        // Здесь должна быть функция отправки данных
        // Все ng-model данных в форме
        //
        // Данные пользователя
        // $scope.firstName - Имя
        // $scope.lastName  - Фамилия
        // $scope.email     - Почтовый Адрес (электронный)
        // $scope.phone     - Номер Телефона
        // $scope.month     - Месяц рождения
        // $scope.day       - День рождения
        // $scope.year      - Год рождения
        // $scope.currentPatient - true / false (radio btn)
        //
        // Offer Code
          // $scope.offerCode - Опционален, проверки на правильность пока нету
          var reSupplyCustomerModel = {
              firstName: $scope.firstName,
              lastName: $scope.lastName,
              email: $scope.email,
              phone: $scope.phone,
              month: $scope.month,
              day: $scope.day,
              year: $scope.year,
              currentPatient: $scope.currentPatient             
          };
          dataService.postReSupply(reSupplyCustomerModel).success(function () {
              //some success behavior 
          });
      }
    };
  }

  // Месяца рождения
  var months = [
    '01 January',
    '02 February',
    '03 March',
    '04 April',
    '05 May',
    '06 June',
    '07 July',
    '08 August',
    '09 September',
    '10 October',
    '11 November',
    '12 December'
  ];
  var days = [];
  for (var i = 1; i < 32; i++) {days.push(i);}
  var years  = [];
  for (var i = 0; i < 120; i++) {years.push(new Date().getFullYear() - i);}

})();

