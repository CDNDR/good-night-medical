(function () {
  'use strict';

  var controllerId = 'rentCatalog';
  angular.module('app').controller(controllerId, ['$scope', rentCatalogCtrl]);

  function rentCatalogCtrl($scope) {
    $scope.machines = machines;

    // Скрывает информацию о машине
    $scope.showMachineCheck = function(index) {
      if (index !== showMachine) {return 'hide-machine';}
    };

    // Если была нажата кнопка "влево"
    $scope.leftMachine = function() {
      showMachine -= 1;
      imgClass[showMachine] = 'enabled';
      imgClass[showMachine + 1] = 'disabled-right';
      $scope.disableBtn();
    };

    // Если была нажата кнопка "вправо"
    $scope.rightMachine = function() {
      showMachine += 1;
      imgClass[showMachine] = 'enabled';
      imgClass[showMachine - 1] = 'disabled-left';
      $scope.disableBtn();
    };

    // Отключаем кнопки "влево" или "вправо" если достигнут предел
    $scope.disableBtn = function() {
      (showMachine == 0) ? $scope.leftDisable = 'disable' : $scope.leftDisable = '';
      (showMachine == 2) ? $scope.rightDisable = 'disable' : $scope.rightDisable = '';
    };

    // Возращаем изображению нужный стиль
    $scope.returnImgClass = function(index) {
      return imgClass[index];
    };
  }

  var showMachine = 1;

  var imgClass = [
    'disabled-left',
    'enabled',
    'disabled-right'
  ];

  var machines = [{
    imgUrl: '//swimages.blob.core.windows.net/goodnightmedical/resmed-s9-auto-cpap.jpg',
    title: 'Refurbished CPAP',
    price: 29,
    ships1: '+2.00',
    ships2: 'S&H',
    desc: 'Our most affordable option, refurbished machines are perfect for users whare currently or have previously undergone therapy. Each machines goes through rigorous 27 point inspection and cleaning',
    lists: [
      'No insurance necessary',
      'Standard pressure capability',
      'The Good Knight’s Guarantee',
      'Humidifier included'
    ],
    uiSref: 'machine.refCpap'
  },{
    imgUrl: '//swimages.blob.core.windows.net/goodnightmedical/resmed-s10-airsense-auto.jpg',
    title: 'New CPAP',
    price: 49,
    ships1: 'ships',
    ships2: 'free',
    desc: 'Looking for a machine insurance will cover? Sometimes insurance requirestarting out on rentals. These machines provide the highest quality care, with datupload capability and advanced pressure titration',
    lists: [
      'Full compliance and efficacy reporting',
      'Auto adjusting pressure',
      'Manufacturer’s Warranty',
      'Covered by most insurance'
    ],
    uiSref: 'machine.newCpap'
  },{
    imgUrl: '//swimages.blob.core.windows.net/goodnightmedical/philips-respironics-dreamstation-bipap.jpg',
    title: 'BiPAP',
    price: 75,
    ships1: 'ships',
    ships2: 'free',
    desc: 'Premium bilevel sleep therapy that automatically adjusts inspiratory anexpiratory pressure on a breath-by-breath basis',
    lists: [
      'Perfect for high pressure settings',
      'Bi-level auto adjusting pressure',
      'Manufacturer’s Warranty',
      'Humidifier included'
    ],
    uiSref: 'machine.newBipap'
  }];

})();

