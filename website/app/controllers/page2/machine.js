(function () {
  'use strict';

  var controllerId = 'rentMachine';
  angular.module('app').controller(controllerId, ['$scope', rentMachineCtrl]);

  function rentMachineCtrl($scope) {
    $scope.refCpap = refCpap;
    $scope.newCpap = newCpap;
    $scope.newBipap = newBipap;
    $scope.machines = machines;
  }

  var refCpap = {
    programName: 'Refurbished CPAP',
    imgUrl: 'resmed-s9-auto-cpap.jpg',
    cost: 'starts at',
    price: 29,
    steps: [{
      bind: 'Email/Fax your valid sleep study and prescription to info@goodnightmedical.com and make sure to specify you are interested in a Refurbished CPAP rental'
    },{
      bind: 'A dedicated service team member will contact you by phone or email to complete the order'
    },{
      bind: 'Check your porch! Your CPAP machine with tubing and filter will be delivered to your door'
    }],
    howItWorks: 'After your completed order of a rental machine through Good Night Medical, we will send your sanitized machine with tubing and filter. Refurbished machines must be rented for a minimum of one (1) month. There is a one-time Set Up charge equal to the monthly fee to retrieve the initial physician order to set up the unit and confirm the validity of the prescription. This fee will be waived if you supply a copy of your sleep study and a scan/photo of your valid, up-to-date presription. If for any reason we cannot provide you with a machine, Good Night Medical will issue a full refund equivalent to the initial fee paid.',
    paragraphs: [{
      title: 'only for refurbished',
      desc: 'Good Knight Guarantee: If somehow your machine breaks down, we will replace the unit with the same or comparable unit.'
    }]
  }

  var newCpap = {
    programName: 'New CPAP',
    imgUrl: 'resmed-s10-airsense-auto.jpg',
    cost: 'starts at',
    price: 49,
    steps: [{
      bind: 'Email/Fax your valid sleep study and prescription to info@goodnightmedical.com and make sure to specify you are interested in a New CPAP rental'
    },{
      bind: 'A dedicated service team member will contact you by phone or email to complete the order'
    },{
      bind: 'Check your porch! Your CPAP machine with tubing and filter will be delivered to your door'
    }],
    howItWorks: 'After your completed order of a rental machine through Good Night Medical, we will send your new machine with tubing and filter. New machines must be rented for a minimum of eight (8) months. There is a one-time set up charge equal to the monthly fee to retrieve the initial physician order to set up the unit and confirm the validity of the prescription. This fee is waived if you are a current customer of Good Night Medical. If for any reason we cannot provide you with a machine, Good Night Medical will issue a full refund equivalent to the initial monthly fee paid. Good Night Medical also offers unlimited telephone support during the life of your rental.',
    paragraphs: [{
      title: '',
      desc: '*The rental amount will be charged until the rental program is returned or purchased. During your rental period, you may purchase the unit from us at any time. If the unit is damaged or lost due to your negligence, you will be obligated to purchase the unit. You may alternately choose to return the damaged machine to us, and for $75 we will diagnose and quote the repair. After the quote, the up-front $75 fee can be applied towards the cost of the  repair, the rental of a different unit, or the purchase of another unit.'
    }]
  }

  var newBipap = {
    programName: 'New BiPAP',
    imgUrl: 'philips-respironics-dreamstation-bipap.jpg',
    cost: 'priced at',
    price: 79,
    steps: [{
      bind: 'Email/Fax your valid sleep study and prescription to info@goodnightmedical.com and make sure to specify you are interested in a BiPAP rental'
    },{
      bind: 'A dedicated service team member will contact you by phone or email to complete the order'
    },{
      bind: 'Check your porch! Your CPAP machine with tubing and filter will be delivered to your door'
    }],
    howItWorks: 'After your completed order of a rental machine through Good Night Medical, we will send your new machine with tubing and filter. New machines must be rented for a minimum of eight (8) months. There is a one-time set up charge equal to the monthly fee to retrieve the initial physician order to set up the unit and confirm the validity of the prescription. This fee is waived if you are a current customer of Good Night Medical. If for any reason we cannot provide you with a machine, Good Night Medical will issue a full refund equivalent to the initial monthly fee paid. Good Night Medical also offers unlimited telephone support during the life of your rental.',
    paragraphs: [{
      title: '',
      desc: '*The rental amount will be charged until the rental program is returned or purchased. During your rental period, you may purchase the unit from us at any time. If the unit is damaged or lost due to your negligence, you will be obligated to purchase the unit. You may alternately choose to return the damaged machine to us, and for $75 we will diagnose and quote the repair. After the quote, the up-front $75 fee can be applied towards the cost of the repair, the rental of a different unit, or the purchase of another unit.'
    }]
  }

  var machines = [{
      model: "ResMed S10 AirSense Auto/Pro",
      imgUrl: "resmed-s10-airsense-auto-pro.jpg",
      price: 65,
      refurbished: false,
      company: "ResMed"
    },{
      model: "ResMed S10 AirSense Auto",
      imgUrl: "resmed-s10-airsense-auto-pro.jpg",
      price: 45,
      refurbished: true,
      company: "ResMed"
    },{
      model: "Philips Respironics DreamStation Auto/Pro",
      imgUrl: "philips-respironics-dreamstation-auto-pro.jpg",
      price: 65,
      refurbished: false,
      company: "Philips"
    },{
      model: "Philips Respironics RemStar System One Auto",
      imgUrl: "philips-respironics-remstar-system-one-auto.jpg",
      price: 45,
      refurbished: true,
      company: "Philips"
    },{
      model: "Fisher & Paykel ICON Premo/Auto",
      imgUrl: "fisher-and-paykel-icon-premo-auto.jpg",
      price: 49,
      refurbished: false,
      company: "Fisher & Paykel"
    },{
      model: "Fisher & Paykel ICON Auto",
      imgUrl: "fisher-and-paykel-icon-auto.jpg",
      price: 29,
      refurbished: true,
      company: "Fisher & Paykel"
    },{
      model: "ResMed S9 Auto/CPAP",
      imgUrl: "resmed-s9-auto-cpap.jpg",
      price: 29,
      refurbished: true,
      company: "ResMed"
    },{
      model: "Philips Respironics System One Auto",
      imgUrl: "philips-respironics-system-one-auto.jpg",
      price: 29,
      refurbished: true,
      company: "Philips"
    },{
      model: "Philips Respironics DreamStation BiPAP",
      imgUrl: "philips-respironics-dreamstation-bipap.jpg",
      price: 75,
      refurbished: false,
      company: "Philips"
    }];

})();

