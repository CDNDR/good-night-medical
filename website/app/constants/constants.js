(function (){
  'use strict';
  var app = angular.module('app');

  app.constant('globalConstants', {
    apiUrl: "http://localhost:51925/api/"
    //apiUrl: "http://goodnight-medical-demo.azurewebsites.net/",
  })
})();
