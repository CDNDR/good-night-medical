angular.module('dataServiceModule', []).factory('dataService', ['$http', 'globalConstants', dataService]);

function dataService($http, globalConstants) {
  var url = globalConstants.apiUrl;

  var service = {
    postContactUs: postContactUs,
    postRent: postRent,
    getRandomMachine: getRandomMachine,
    getMachinesCatalog: getMachinesCatalog,
    postPayment: postPayment,
    postReSupply: postReSupply
  };
  
  function postContactUs(model) {
    var destinationUrl = url + 'customer/contact-us/';

    return $http.post(destinationUrl, model);
  }

  function postRent(model) {
    var destinationUrl = url + 'customer/rent';

    return $http.post(destinationUrl, model)
  }

  function getRandomMachine() {
    var destinationUrl = url + 'machines/random/';

    return $http.get(destinationUrl);
  }

  function getMachinesCatalog() {
      var destinationUrl = url + 'machines';

      return $http.get(destinationUrl);
  }

  function postPayment(model) {
      var destinationUrl = url + 'api/payment';
      return $http.post(destinationUrl, model);
  }

  function postReSupply(model) {
      var destinationUrl = url + 'resupply';
      return $http.post(destinationUrl, model);
  }


  return service;
}