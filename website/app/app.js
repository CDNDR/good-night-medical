(function() {
  'use strict';

  var app = angular.module('app', [
    'ngAnimate',
    'ui.router',
    'ui.slider',
    'text.ellipsis',
    'angular-loading-bar',
    'dataServiceModule',
    'shareServiceModule'
  ])
    .config(function($animateProvider) {
        // Анимация для определенных элементов (ngAnimate)
        $animateProvider.classNameFilter(/animate-on/);
    });

  // Handle routing errors and success events
  app.run([function () {
    // Include $route to kick start the router.
  }]);

})();
