module.exports = function(config) {
    config.set({
        frameworks: ['jasmine'],
        browsers: ['Chrome'],
        files: [
            'controllersTests/mainTests.js',
            'controllersTests/common/billingTests.js',
            'controllersTests/common/thanksTests.js',
            'controllersTests/modules/footerTests.js'
        ]
    });};