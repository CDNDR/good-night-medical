var app = angular.module('app');

app.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise("/");

  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'app/templates/page1.html',
      onEnter: function() {window.scrollTo(0,0);}
    })
    .state('rent', {
      url: '/rent',
      templateUrl: 'app/templates/page2.html'
    })
      .state('rent.catalog', {
        url: '/catalog',
        templateUrl: 'app/views/page2/catalog.html',
        onEnter: function() {window.scrollTo(0,0);}
      })
      .state('rent.machine', {
        url: '/machine',
        templateUrl: 'app/views/page2/machine.html',
        onEnter: function() {window.scrollTo(0,0);}
      })
    .state('reSupply', {
      url: '/re-supply',
      templateUrl: 'app/templates/page3.html'
    })
      .state('reSupply.info', {
        url: '/info',
        templateUrl: 'app/views/page3/info.html',
        onEnter: function() {window.scrollTo(0,0);}
      })
      .state('reSupply.form', {
        url: '/form',
        templateUrl: 'app/views/page3/form.html',
        onEnter: function() {window.scrollTo(0,0);}
      })
    .state('catalog', {
      url: '/catalog',
      templateUrl: 'app/templates/page4.html'
    })
      .state('catalog.cpap', {
        url: '/cpap',
        templateUrl: 'app/views/page4/cpap.html',
        onEnter: function() {window.scrollTo(0,0);}
      })
      .state('catalog.help', {
        url: '/help',
        templateUrl: 'app/views/page4/help.html',
        onEnter: function() {window.scrollTo(0,0);}
      })
    .state('sleepTest', {
      url: '/sleep-test',
      templateUrl: 'app/templates/page5.html'
    })
      .state('sleepTest.info', {
        url: '/info',
        templateUrl: 'app/views/page5/info.html',
        onEnter: function() {window.scrollTo(0,0);}
      })
    .state('information', {
      url: '/information',
      templateUrl: 'app/templates/information.html'
    })
      .state('information.aboutUs', {
        url: '/about-us',
        templateUrl: 'app/views/information/about-us.html',
        onEnter: function() {window.scrollTo(0,0);}
      })
      .state('information.locations', {
        url: '/locations',
        templateUrl: 'app/views/information/locations.html',
        onEnter: function() {window.scrollTo(0,0);}
      })
      .state('information.testimonials', {
        url: '/testimonials',
        templateUrl: 'app/views/information/testimonials.html',
        onEnter: function() {window.scrollTo(0,0);}
      })
      .state('information.screeningTool', {
        url: '/screening-tool',
        templateUrl: 'app/views/information/screening-tool.html',
        onEnter: function() {window.scrollTo(0,0);}
      })
    .state('common', {
      url: '/common',
      templateUrl: 'app/templates/common.html'
    })
      .state('common.billing', {
        url: '/billing',
        templateUrl: 'app/views/common/billing.html',
        onEnter: function() {window.scrollTo(0,0);}
      })
      .state('common.thanks', {
        url: '/thanks',
        templateUrl: 'app/views/common/thanks.html',
        onEnter: function() {window.scrollTo(0,0);}
      })
    .state('testing', {
      url: '/testing',
      templateUrl: 'app/templates/testing.html'
    })
      .state('testing.page1', {
        url: '/page1',
        templateUrl: 'app/views/testing/page1.html',
        onEnter: function() {window.scrollTo(0,0);}
      })
});
