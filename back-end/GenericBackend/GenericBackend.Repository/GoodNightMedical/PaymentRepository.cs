﻿using GenericBackend.DataModels.GoodNightMedical;

namespace GenericBackend.Repository.GoodNightMedical
{
    public class PaymentRepository : MongoRepository<PaymentModel>, IMongoRepository<PaymentModel>
    {
    }
}
