﻿using GenericBackend.DataModels.GoodNightMedical;

namespace GenericBackend.Repository.GoodNightMedical
{
    public class PatientRepository : MongoRepository<Patient>, IMongoRepository<Patient>
    {

    }
}
