﻿
namespace GenericBackend.Models
{
    public class ReSupplyCustomerModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Month { get; set; }
        public string Day { get; set; }
        public string Year { get; set; }
        public bool CurrentPatient { get; set; }
        public string OfferCode { get; set; }
    }
}
