﻿using System;
using System.Web.Http;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.PaymentProcessor.Core.Interfaces;
using GenericBackend.Repository;
using GenericBackend.UnitOfWork.GoodNightMedical;
using PaymentModel = GenericBackend.Models.Payment.PaymentModel;

namespace GenericBackend.Controllers
{
    [RoutePrefix("api/payment")]
    public class PaymentController : ApiController
    {
        private readonly IPaymentProcessor _paymentProcessor;
        private readonly IMongoRepository<DataModels.GoodNightMedical.PaymentModel> _paymentRepository;

        public PaymentController(IPaymentProcessor paymentProcessor,IUnitOfWork unitOfWork)
        {
            _paymentProcessor = paymentProcessor;
            _paymentRepository = unitOfWork.Payment;
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult Post([FromBody]PaymentModel model)
        {
            _paymentProcessor.SetCreditCard(model.CreditCard);
            _paymentProcessor.SetShippingBillingAddress(model.Shipment, model.IsSameBillShip ? model.Shipment : model.Billing);
            _paymentProcessor.InitializeChargeRequestAndExecute(model.Type, Convert.ToDecimal(model.Price));

            _paymentRepository.Add(ConvertFromModelToDataModel(model));

            return Ok();
        }

        private DataModels.GoodNightMedical.PaymentModel ConvertFromModelToDataModel(PaymentModel model)
        {
            return new DataModels.GoodNightMedical.PaymentModel
            {
                Shipment = new ShipmentModel
                {
                    FirstName = model.Shipment.FirstName,
                    LastName = model.Shipment.LastName,
                    PrimaryAddress = model.Shipment.PrimaryAddress,
                    SecondaryAddress = model.Shipment.SecondaryAddress,
                    City = model.Shipment.City,
                    State = model.Shipment.State,
                    Zip = model.Shipment.Zip
                },
                CreditCard = new CreditCardModel
                {
                    CardNumber = model.CreditCard.CardNumber,
                    ExpirationMonth = model.CreditCard.ExpirationMonth,
                    ExpirationYear = model.CreditCard.ExpirationYear,
                    Cvv = model.CreditCard.Cvv
                },
                Billing = new ShipmentModel
                {
                    FirstName = model.Billing.FirstName,
                    LastName = model.Billing.LastName,
                    PrimaryAddress = model.Billing.PrimaryAddress,
                    SecondaryAddress = model.Billing.SecondaryAddress,
                    City = model.Billing.City,
                    State = model.Billing.State,
                    Zip = model.Billing.Zip
                },
                IsSameBillShip = model.IsSameBillShip

            };
        }
    }
}
