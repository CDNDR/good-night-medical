﻿using System.Threading.Tasks;
using System.Web.Http;
using GenericBackend.UnitOfWork.GoodNightMedical.Interfaces;

namespace GenericBackend.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/patient")]
    public class PatientController : ApiController
    {
        private readonly IPatientWork _patientWork;

        public PatientController(IPatientWork patientWork)
        {
            _patientWork = patientWork;
        }

        [HttpGet]
        [Route("list")]
        public async Task<IHttpActionResult> Get()
        {
            var patients = await _patientWork.Patients.GetList();

            return Ok(patients);
        }

    }
}
