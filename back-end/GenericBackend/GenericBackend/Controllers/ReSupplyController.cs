﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Http;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.Models;
using GenericBackend.Repository;
using GenericBackend.UnitOfWork.GoodNightMedical;

namespace GenericBackend.Controllers
{
    [RoutePrefix("resupply")]
    public class ReSupplyController : ApiController
    {
        private readonly IMongoRepository<ReSupplyCustomer> _reSupplyCustomerRepository;

        public ReSupplyController(IUnitOfWork unitOfWork)
        {
            _reSupplyCustomerRepository = unitOfWork.ReSupplyCustomer;
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult Post(ReSupplyCustomerModel model)
        {
            _reSupplyCustomerRepository.Add(ConvertFromModelToDataModel(model));
            return Ok();
        }

        private ReSupplyCustomer ConvertFromModelToDataModel(ReSupplyCustomerModel model)
        {
            return new ReSupplyCustomer
            {
                // если CurrentPatient - видимо подгрузить его из базы, если нет создать нового
                Customer = new Patient
                {
                    Email = model.Email,
                    FullName = string.Format(CultureInfo.CurrentCulture, "{0} {1} ", model.FirstName, model.LastName),
                    PhoneNumber = model.Phone,
                    Address = new List<Address>(),
                    DateOfBirth = new DateTime(int.Parse(model.Year), _months[model.Month], int.Parse(model.Day))
                },
                CurrentPatient=model.CurrentPatient,
                OfferCode = model.OfferCode
            };
        }


        private readonly Dictionary<string, int> _months = new Dictionary<string, int>
        {
            { "01 January",1},
            {"02 February",2},
            {"03 March",3},
            {"04 April",4},
            {"05 May",5},
            {"06 June",6},
            {"07 July",7},
            {"08 August",8},
            {"09 September",9},
            {"10 October",10},
            {"11 November",11},
            {"12 December",12}
        };
    }

}