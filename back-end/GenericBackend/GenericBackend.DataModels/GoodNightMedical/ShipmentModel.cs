﻿using GenericBackend.Core;

namespace GenericBackend.DataModels.GoodNightMedical
{
    public class ShipmentModel : MongoEntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimaryAddress { get; set; }
        public string SecondaryAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }
}
