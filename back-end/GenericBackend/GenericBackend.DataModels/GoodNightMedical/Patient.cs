﻿using System;
using System.Collections.Generic;
using GenericBackend.Core;

namespace GenericBackend.DataModels.GoodNightMedical
{
    public class Patient : MongoEntityBase
    {
        public string Email { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public List<Address> Address { get; set; }
        public DateTime DateOfBirth { get; set; }
    }

    public class Address
    {
        public string PlainAddress { get; set; }
        public bool Preferrable { get; set; }
    }
}
