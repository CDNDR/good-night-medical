﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericBackend.Core;

namespace GenericBackend.DataModels.GoodNightMedical
{
    public class PaymentModel: MongoEntityBase
    {
        public ShipmentModel Shipment { get; set; }
        public CreditCardModel CreditCard { get; set; }
        public bool IsSameBillShip { get; set; }
        public ShipmentModel Billing { get; set; }
        public string Type { get; set; }
        public string Price { get; set; }
    }
}
