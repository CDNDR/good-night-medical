﻿using GenericBackend.Core;

namespace GenericBackend.DataModels.GoodNightMedical
{
    public class ReSupplyCustomer : MongoEntityBase
    {
        public Patient Customer { get; set; }
        public bool CurrentPatient { get; set; }
        public string OfferCode { get; set; }
    }
}
