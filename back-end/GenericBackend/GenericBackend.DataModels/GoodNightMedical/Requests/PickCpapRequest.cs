﻿using GenericBackend.Core;

namespace GenericBackend.DataModels.GoodNightMedical.Requests
{
    public class PickCpapRequest : MongoEntityBase
    {
        public string PatientId { get; set; }
        public string Message { get; set; }
    }
}
