﻿using GenericBackend.DataModels.GoodNightMedical.Requests;

namespace GenericBackend.UnitOfWork.GoodNightMedical.Models.Patient
{
    public class HelpPickCpap
    {
        public PickCpapRequest PickCpapRequest { get; set; }
        public DataModels.GoodNightMedical.Patient Patient { get; set; }

    }
}
