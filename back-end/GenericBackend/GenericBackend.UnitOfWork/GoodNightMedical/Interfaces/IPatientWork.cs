﻿using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.DataModels.GoodNightMedical.Requests;
using GenericBackend.Repository;

namespace GenericBackend.UnitOfWork.GoodNightMedical.Interfaces
{
    public interface IPatientWork : IUnitOfWork
    {
        IMongoRepository<Patient> Patients { get; }
        IMongoRepository<PickCpapRequest> PickCpapRequests { get; }

        string UpdatePatientInfo(Patient patient);
    }
}
